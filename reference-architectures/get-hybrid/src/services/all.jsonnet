[
  import 'consul.jsonnet',
  import 'gitaly.jsonnet',
  import 'gitlab-shell.jsonnet',
  import 'logging.jsonnet',
  import 'praefect.jsonnet',
  import 'registry.jsonnet',
  import 'sidekiq.jsonnet',
  import 'webservice.jsonnet',
]
